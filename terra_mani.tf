resource"aws_vpc" "vpc_terra" {
    cidr_block = "10.0.0.0/16"
    enable_classiclink = "false"
    instance_tenancy = "default"    
    
    tags = {
        Name = "vpc_terra"
    }
}
resource "aws_subnet" "terra-subnet" {
    vpc_id = "${aws_vpc.vpc_terra.id}"
    cidr_block = "10.0.1.0/24"
    map_public_ip_on_launch = "true" 
    availability_zone = "eu-west-2a"

     tags = {
        Name = "terra-subnet"
    }
}

resource "aws_internet_gateway" "terra_igw" {
    vpc_id = "${aws_vpc.vpc_terra.id}"

    tags = {
        Name = "terra_igw"
    }
}
terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 3.27"
    }
  }
}

provider "aws" {
  profile = "default"
  region  = "eu-west-2"
}

resource "aws_instance" "Ec2_terra" {
  ami           = "ami-096cb92bb3580c759"
  instance_type = "t2.micro"
  subnet_id = "${aws_subnet.terra-subnet.id}"
  security_groups = ["${aws_security_group.allow_tcp.id}"]
  key_name = "London_terra"

  tags = {
    Name = "Ec2_terra"
  }
}

  resource "aws_security_group" "allow_tcp" {
  vpc_id      = "${aws_vpc.vpc_terra.id}"

  ingress {
    description = "TCP from VPC"
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"] 
    }

  tags = {
    Name = "allow_tcp"
  }
}

